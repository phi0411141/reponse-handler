class CustomError extends Error {
	constructor(params) {
		super();
		this.message = params.message;
		Error.captureStackTrace(this, CustomError);
		this.errCode = params.code;
	}
}

module.exports = CustomError;
