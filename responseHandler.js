class ResponseHandler {
	constructor(errList) {
		this.errList = errList;
	}

	error(res, error, message) {
		return res.status(this.errList[error].code).json({
			success: false,
			error: message || this.errList[error].message,
		});
	}

	success(res, result) {
		return res.json({
			data: { ...result },
			success: true,
		})
	}
}

module.exports = ResponseHandler;


